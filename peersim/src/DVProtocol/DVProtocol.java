package DVProtocol;

import java.util.TreeMap;

import DVProtocol.Path;
import peersim.cdsim.CDProtocol;
import peersim.config.FastConfig;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.vector.SingleValueHolder;

public class DVProtocol extends SingleValueHolder implements CDProtocol {

	// you can change any of these with justification
	public static final int INIT = 0;
	public static final int BCST = 1;
	public static final int STOP = 2;
	private int stage;
	private int hits;
	//private boolean updates;
	private TreeMap<Long, Path> netMap; // you can change this to list if you like

	public DVProtocol(String prefix) {
		super(prefix);
		stage = INIT;
		hits = 0;
	}

	@Override
	public void nextCycle(Node host, int protocolID) 
	{
		 Linkable lnk = (Linkable) host.getProtocol(FastConfig.getLinkable(protocolID));
	        /* Reference host's node ID */
	        /* Current phase */
	        switch (stage) {
	            case INIT:
	            	Node neighbor;
	            	netMap = new TreeMap<Long, Path>();
	            	 netMap.put(host.getID(), new Path(host.getID(), host.getID(), 0));
	            	 for (int i = 0; i < lnk.degree(); i++) {
	            		 neighbor = lnk.getNeighbor(i);
	            		 netMap.put(neighbor.getID(), new Path(neighbor.getID(), neighbor.getID(), 1));
	            		 }
	                break;
	            case BCST:
	            	for (int i = 0; i < lnk.degree(); i++) {
	            		neighbor = lnk.getNeighbor(i);
	            		DVProtocol protocol = (DVProtocol) neighbor.getProtocol(protocolID);
	            		TreeMap<Long, Path> msg = new TreeMap<Long, Path>();
	            		for (Path p : netMap.values()) {
	            		// add this as predecessor node
	            		msg.put(p.destination, new Path(p.destination, host.getID(), p.hops + 1));
	            		}
	            		protocol.update(msg);
	            		msg = null;
	            	}
	                break;
	            case STOP:
	            	
	               // updates(host.getID());
	                break;
	        }
	}

	public void update(TreeMap<Long, Path> msg) 
	{
		
		for (Path p : msg.values()) {
			 // destination node is not in local map, add it, use sender node as predecessor
			 if (!netMap.containsKey(p.destination)) {
			 netMap.put(p.destination, new Path(p.destination, p.predecessor, p.hops));
			 }
			}
		
		for (Path dst : netMap.values()) {
			int ths2dst = dst.hops;
			int nbr2dst = Integer.MAX_VALUE;
			if (msg.containsKey(dst.destination))
				nbr2dst = msg.get(dst.destination).hops;
			if (nbr2dst < ths2dst){
				dst.hops = nbr2dst;
				dst.predecessor = msg.get(dst.destination).predecessor;
				}
			}
		}
	

	public TreeMap<Long, Path> getNetMap() {
		return netMap;
	}

	public boolean hasUpdates() {
		return hits < 3;
	}
}
